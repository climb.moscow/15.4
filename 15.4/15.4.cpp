﻿#include <iostream>
#include <locale.h>

void MyFunction(int, int);

int main()
{
    setlocale(LC_ALL, "Russian");
    int a,b;
    std::cout << "Если хочешь увидеть четные вводи 2, если не четные 1! ";
    std::cin >> a;
    std::cout << "До какого числа выводим? ";
    std::cin >> b;
    MyFunction(a, b);
}

void MyFunction(int a, int b)
{
    if(a%2==0)
        for (int i = 0; i <= b; i++)
        {
            if (i % 2 == 0) std::cout << i << " " << std::endl;
        }
    else
        for (int i = 0; i <= b; i++)
        {
            if (i % 2 != 0) std::cout << i << " " << std::endl;
        }
}